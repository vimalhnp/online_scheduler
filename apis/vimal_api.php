<?php

$conn = mysql_connect("localhost", "online_scheduler", "online_scheduler");
mysql_select_db("online_scheduler");
extract($_REQUEST);
$site_url = $_SERVER['HTTP_HOST'] . "/apis/";
if (isset($_REQUEST['action']) && $action == 'login') {
    $user = array();
    if (!isset($username) || $username == '') {
        $out = array(
            'ERROR' => 1,
            'DATA' => 'Username Blank',
            "MESSAGE" => "Please Enter Username"
        );
    }
    if (!isset($password) || $password == '') {
        $out = array(
            'ERROR' => 1,
            'DATA' => 'Password Blank',
            "MESSAGE" => "Please Enter Password"
        );
    }

    $qry1 = "select * from user_mst WHERE username='$username' AND password='" . ($password) . "'";
    $result1 = mysql_query($qry1);
    $cnt = mysql_num_rows($result1);
    if (intval($cnt) > 0) {
        $res = mysql_fetch_array($result1);
        $out = array(
            'ERROR' => 0,
            'DATA' => 'Success',
            "MESSAGE" => "Successfully Login",
            "user_id" => $res['user_id']
        );
    } else {
        $out = array(
            'ERROR' => 1,
            'DATA' => 'Invalid',
            "MESSAGE" => "invalid Username or password"
        );
    }

    echo json_encode($out);
    exit();
} elseif (isset($_REQUEST['action']) && $action == 'register') {
    $user = array();
    if (!isset($username) || $username == '') {
        $out = array(
            'ERROR' => 1,
            'DATA' => 'Username Blank',
            "MESSAGE" => "Please Enter Username"
        );
    }
    if (!isset($password) || $password == '') {
        $out = array(
            'ERROR' => 1,
            'DATA' => 'Password Blank',
            "MESSAGE" => "Please Enter Password"
        );
    }
    if (!isset($email_id) || $email_id == '') {
        $out = array(
            'ERROR' => 1,
            'DATA' => 'Email ID must not blank',
            "MESSAGE" => "Please Enter Email ID"
        );
    }
    if (!isset($profile_pic) || $profile_pic == '') {
        $out = array(
            'ERROR' => 1,
            'DATA' => 'Profile Photo must not blank',
            "MESSAGE" => "Please Choose any photo"
        );
    }

    // Get image string posted from Android App
    $base = $_REQUEST['profile_pic'];
    // Get file name posted from Android App
    $filename = "user_" . time() . ".png";
    // Decode Image
    $binary = base64_decode($base);
    header('Content-Type: bitmap; charset=utf-8');
    // Images will be saved under 'www/imgupload/uplodedimages' folder
    $filepath = 'users/' . $filename;
    $file = fopen($filepath, 'wb');
    // Create File
    fwrite($file, $binary);
    fclose($file);


    $qry1 = "insert into car_user_mst SET username='$username' , password='" . ($password) . "', email_id='" . $email_id . "',contact_no='" . $contact_no . "'";
    $result1 = mysql_query($qry1);
    $id = mysql_insert_id();
    if (intval($id) > 0) {
        $out = array(
            'ERROR' => 0,
            'DATA' => 'Success',
            "MESSAGE" => "Successfully Registered",
            "user_id" => $id,
            "profile_pic" => $site_url . $filepath
        );
    } else {
        $out = array(
            'ERROR' => 1,
            'DATA' => 'Invalid',
            "MESSAGE" => "OOPS,Not registered!"
        );
    }

    echo json_encode($out);
    exit();
} elseif (isset($_REQUEST['action']) && $action == 'get_list') {
    $user = array();
    $qry1 = "SELECT * FROM  `products` WHERE status=1 ";
    $result1 = mysql_query($qry1);
    $outArr = array();
    while ($row = mysql_fetch_array($result1)) {
        $outArr[] = array(
            'name' => $row['name'],
            'image' => $row['image'],
            'link' => $row['link']
        );
    }

    if (count($outArr) > 0) {
        $out = array(
            'ERROR' => 0,
            'DATA' => $outArr,
            "MESSAGE" => "Successfully Registered",
        );
    } else {
        $out = array(
            'ERROR' => 1,
            'DATA' => 'Invalid',
            "MESSAGE" => "OOPS,Not recognised!"
        );
    }
    echo json_encode($out);
    exit();
} elseif (isset($_REQUEST['action']) && $action == 'get_notice_list') {
    $user = array();
    $qry1 = "SELECT * FROM  `notification_mst` WHERE status=1 AND DATE(end_date) >= '" . date('Y-m-d') . "'";
    $result1 = mysql_query($qry1);
    $outArr = array();
    while ($row = mysql_fetch_array($result1)) {
        $outArr[] = array(
            'title' => $row['title'],
            'message' => $row['message'],
            'frequency' => $row['frequency']
        );
    }

    if (count($outArr) > 0) {
        $out = array(
            'ERROR' => 0,
            'DATA' => $outArr,
            "MESSAGE" => "Successfully Registered",
        );
    } else {
        $out = array(
            'ERROR' => 1,
            'DATA' => 'Invalid',
            "MESSAGE" => "OOPS,Not recognised!"
        );
    }
    echo json_encode($out);
    exit();
} else if (isset($_REQUEST['action']) && $action == 'get_feedback_list') {
    $user = array();
    $qry2 = "SELECT * FROM  `feedback_mst` WHERE status=1 ";
    $result2 = mysql_query($qry2);
    $outArr = array();
    while ($row = mysql_fetch_array($result2)) {
        $outArr[] = array(
            'title' => $row['title'],
            'content' => $row['content']
        );
    }

    if (count($outArr) > 0) {
        $out = array(
            'ERROR' => 0,
            'DATA' => $outArr,
            "MESSAGE" => "Successfull",
        );
    } else {
        $out = array(
            'ERROR' => 1,
            'DATA' => 'Invalid',
            "MESSAGE" => "OOPS,Not recognised!"
        );
    }
    echo json_encode($out);
    exit();
} elseif (isset($_REQUEST['action']) && $action == 'get_app_list') {
    $user = array();
    $qry3 = "SELECT * FROM  `appoinment_mst` WHERE is_approve=1";
    $result3 = mysql_query($qry3);
    $outArr = array();
    while ($row = mysql_fetch_array($result3)) {
        $outArr[] = array(
            'title' => $row['title'],
            'details' => $row['details']
        );
    }

    if (count($outArr) > 0) {
        $out = array(
            'ERROR' => 0,
            'DATA' => $outArr,
            "MESSAGE" => "Successfully Registered",
        );
    } else {
        $out = array(
            'ERROR' => 1,
            'DATA' => 'Invalid',
            "MESSAGE" => "OOPS,Not recognised!"
        );
    }
    echo json_encode($out);
    exit();
} elseif (isset($_REQUEST['action']) && $action == 'get_timetable') {
    extract($_REQUEST);
    if (!isset($user_id) || $user_id == '') {
        $out = array(
            'ERROR' => 1,
            'DATA' => 'User Id Blank',
            "MESSAGE" => "Please Enter User Id"
        );
    } else {

        $qry3 = "SELECT * FROM  `lecture_schedule_mst`WHERE status=1 AND user_id='" . $user_id . "'";
        $result3 = mysql_query($qry3);
        $outArr = array();
        while ($row = mysql_fetch_array($result3)) {
            $outArr[] = array(
                'subject' => $row['subject'],
                'lecture_day' => $row['lecture_day'],
                'lecture_time' => $row['lecture_time'],
                'class_standard' => $row['class_standard'],
                'room_no' => $row['room_no'],
                'department' => $row['department']
            );
        }

        if (count($outArr) > 0) {
            $out = array(
                'ERROR' => 0,
                'DATA' => $outArr,
                "MESSAGE" => "Successfully Retrieved",
            );
        } else {
            $out = array(
                'ERROR' => 1,
                'DATA' => 'Invalid',
                "MESSAGE" => "OOPS,Not recognised!"
            );
        }
    }
    echo json_encode($out);
    exit();
}
elseif (isset($_REQUEST['action']) && $action == 'get_change_pass') {
    extract($_REQUEST);
    if (!isset($email) || $email == '') {
        $out = array(
            'ERROR' => 1,
            'DATA' => 'Email ID Blank',
            "MESSAGE" => "Please Enter EmailId"
        );
    } else {

        $qry4 = "SELECT * FROM  `user_mst`WHERE status=1 AND email_id='" . $email . "'";
		
		$result4 = mysql_query($qry4);
        $outArr = array();
        while ($row = mysql_fetch_array($result4)) {
            $outArr[] = array(
                'user_id' => $row['user_id']
            );
        }
//print_r($outArr);exit;
        if (count($outArr) > 0) {
            $out = array(
                'ERROR' => 0,
                'DATA' => $outArr,
                "MESSAGE" => "Successfully Retrieved",
            );
        } else {
            $out = array(
                'ERROR' => 1,
                'DATA' => 'Invalid',
                "MESSAGE" => "OOPS,Not recognised!"
            );
        }
		
    }
    echo json_encode($out);
    exit(); 
	}
	elseif (isset($_REQUEST['action']) && $action == 'get_update_pass') {
    extract($_REQUEST);
    if (!isset($user_id) || $user_id == '') {
        $out = array(
            'ERROR' => 1,
            'DATA' => 'user_id Blank',
            "MESSAGE" => "Please Enter user_id"
        );
    } else {

       $qry5="update user_mst set password='".$password."' where user_id='".$user_id."'";	
		$result5 = mysql_query($qry5);
        $outArr = array(0=>$user_id);
        
//print_r($outArr);exit;
        if (count($outArr) > 0) {
            $out = array(
                'ERROR' => 0,
                'DATA' => $outArr,
                "MESSAGE" => "Successfully Retrieved",
            );
        } else {
            $out = array(
                'ERROR' => 1,
                'DATA' => 'Invalid',
                "MESSAGE" => "OOPS,Not recognised!"
            );
        }
		
    }
    echo json_encode($out);
    exit(); 
	}
else {
    $out = array(
        'ERROR' => 1,
        'DATA' => 'Invalid',
        "MESSAGE" => "invalid action"
    );
    echo json_encode($out);
    exit();
}
?>
