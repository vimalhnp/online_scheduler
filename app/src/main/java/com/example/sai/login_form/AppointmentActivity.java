package com.example.sai.login_form;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sai.login_form.services.Services;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by sai on 1/26/2016.
 */
public class AppointmentActivity extends Activity {
    private ProgressDialog dialog;
    public ListView applistview;
    public String[] title,details;
    int loader = R.drawable.home;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);

//        (WindowManager.LayoutParams.FLAG_FULLSCREEN), WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_appointment_table);
        new AsyncTaskNotice().execute();
    }
    private class MySimpleArrayAdapter extends ArrayAdapter<String> {
        private final Context context;
        private String[] title = new String[0];
//        private final String[] message;

        public MySimpleArrayAdapter(Context context, String[] title) {
            super(context, R.layout.appointment_list, title);
            this.context = context;
            this.title = title;
//            this.message = message;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.appointment_list, parent, false);
            TextView textView = (TextView) rowView.findViewById(R.id.app_label);
//            ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
            if (title[position] == null || details[position] == null) {
                // do nothing
//                Log.d("error", "error1");
            } else {
                textView.setText(title[position]);

//                ImageLoader imgLoader = new ImageLoader(getApplicationContext());
//                imgLoader.DisplayImage(message[position], loader, imageView);

//                imageView.setImageResource(R.drawable.result);
            }
            return rowView;
        }
    }

    private class AsyncTaskNotice extends
            AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = new ProgressDialog(getContext());
            dialog.setMessage("Authenticating, Please wait...");
            dialog.setIndeterminate(false);
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.setProgress(0);
            int apiLevel = android.os.Build.VERSION.SDK_INT;
            if (apiLevel >= 11) {
                dialog.setProgressNumberFormat(null);
            }
            dialog.show();
        }


        @Override
        protected String doInBackground(Void... params) {
            String response = Services.getProducts(AppointmentActivity.this, "get_app_list");

            System.out.println(response);

            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            Log.d("onPostExecute ", response);
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            fillProducts(response);
        }
    }

    protected Context getContext() {
        return this;
    }

    public void fillProducts(String response) {
        if (response != null && !response.equals("")) {
            try {
                Log.d("abc 1 ", response);
                JSONObject jsonobj = new JSONObject(response);
                if (jsonobj.has("ERROR")) {
                    Log.d("abc 2 ", response);
                    if (jsonobj.getString("ERROR").equalsIgnoreCase("0")) {
                        Log.d("abc 3 ", response);
                        if (jsonobj.has("DATA")) {
                            Log.d("abc 4 ", response);
                            JSONArray myProducts = jsonobj.getJSONArray("DATA");

                            title = new String[myProducts.length()];
                            details = new String[myProducts.length()];
                            for (int i = 0; i < myProducts.length(); i++) {
                                JSONObject tempjo = new JSONObject();
                                tempjo = myProducts.getJSONObject(i);
                                title[i] = tempjo.getString("title");
                                details[i] = tempjo.getString("details");
//                                links[i]= myProducts.getJSONObject(i).getString("link");
//                                images[i]= myProducts.getJSONObject(i).getString("image");
                            }
                            Log.d("abc 11","hi");
                            Log.d("abc 5 ", title.toString());
                            applistview = (ListView) findViewById(R.id.app_listview);

                            final MySimpleArrayAdapter adapter = new MySimpleArrayAdapter(AppointmentActivity.this, title);
                            applistview.setAdapter(adapter);

                            applistview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                                @Override
                                public void onItemClick(AdapterView<?> parent, final View view,
                                                        int position, long id) {
                                    // this is when we click on item of any list view
                                    SharedPreferences sharedPrefs = getContext().getSharedPreferences("app_data", 0);
                                    SharedPreferences.Editor e = sharedPrefs.edit();
                                    e.putString("title", title[position]);
                                    e.putString("details", details[position]);
                                    e.commit();
                                    Intent app_detail = new Intent(AppointmentActivity.this, AppointmentDetailsActivity.class);
                                    startActivity(app_detail);
                                }

                            });
                        }
                    } else {
                        Toast t = Toast.makeText(AppointmentActivity.this, "No Products Found", Toast.LENGTH_SHORT);
                        t.show();
                    }
                } else {
                    Log.d("vimal 6", response);
                    Toast t = Toast.makeText(AppointmentActivity.this, "OOPS, Something went wrong!", Toast.LENGTH_SHORT);
                    t.show();
                }
            } catch (Exception e) {
                e.printStackTrace();

            }
        }
        if (dialog != null) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        }
    }
}