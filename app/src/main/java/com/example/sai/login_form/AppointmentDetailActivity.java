package com.example.sai.login_form;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.melnykov.fab.FloatingActionButton;

/**
 * Created by SAI on 2/7/2016.
 */
public class AppointmentDetailActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences sharedPrefs = getContext().getSharedPreferences("app_data", 0);

        String detail_a_title = sharedPrefs.getString("title", "");
        String detail_a_content = sharedPrefs.getString("content", "");
        String detail_user_type = sharedPrefs.getString("user_type", "");
        String detail_give_time = sharedPrefs.getString("give_time", "");
        String detail_take_time = sharedPrefs.getString("take_time", "");

        //Remove title bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_appointment_detail);

        TextView a_title=(TextView)findViewById(R.id.a_title_txt);
        a_title.setText(detail_a_title);

        TextView a_content=(TextView)findViewById(R.id.a_content_txt);
        a_content.setText(detail_a_content);

        TextView user_type=(TextView)findViewById(R.id.user_type_txt);
        user_type.setText(detail_user_type);

        TextView give_time=(TextView)findViewById(R.id.give_time_txt);
        give_time.setText(detail_give_time);

        TextView take_time=(TextView)findViewById(R.id.take_time_txt);
        take_time.setText(detail_take_time);

//
//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                NoticeDetailActivity.this.finish();
//            }
//        });
    }
    protected Context getContext() {
        return this;
    }
}
