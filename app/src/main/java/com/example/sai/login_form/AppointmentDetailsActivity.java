package com.example.sai.login_form;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

public class AppointmentDetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences sharedPrefs = this.getSharedPreferences("app_data", 0);
        String detail_title = sharedPrefs.getString("title", "");
        String details = sharedPrefs.getString("details", "");

        setContentView(R.layout.activity_appointment_details);

        Toolbar app_Detail = (Toolbar) findViewById(R.id.appointment);
        setSupportActionBar(app_Detail);

        CollapsingToolbarLayout noticeLayout = (CollapsingToolbarLayout) findViewById(R.id.app_layout);
        noticeLayout.setTitle(sharedPrefs.getString("title",""));

        TextView App_detail=(TextView)findViewById(R.id.appointment_text);
        App_detail.setText(sharedPrefs.getString("details",""));

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.app_fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }
}
