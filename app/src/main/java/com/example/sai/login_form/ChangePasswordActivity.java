package com.example.sai.login_form;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.*;

import com.example.sai.login_form.services.Services;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by sai on 05/24/2016.
 */
public class ChangePasswordActivity extends Activity {

    public String newPassword, confirmPassword;
    private ProgressDialog dialog;
    String userid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        SharedPreferences sharedPrefs = getContext().getSharedPreferences("changepass_details", 0);
        userid = sharedPrefs.getString("user_id", "");

        if (userid.equalsIgnoreCase("")) {
            Toast t = Toast.makeText(ChangePasswordActivity.this, "Your email id not registered!", Toast.LENGTH_SHORT);
            t.show();
            Intent dash = new Intent(ChangePasswordActivity.this, ForgotPasswordActivity.class);
            startActivity(dash);
            finish();
        }

        Button changebtn = (Button) findViewById(R.id.saveChanges);
        changebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText np = (EditText) findViewById(R.id.new_pass);
                newPassword = np.getText().toString();

                EditText cp = (EditText) findViewById(R.id.confirm_pass);
                confirmPassword = cp.getText().toString();

                if (userid == "" || userid.equalsIgnoreCase("")) {

                    Toast.makeText(getApplicationContext(), "Please Enter Registered EmailID", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (newPassword == "" || newPassword.equalsIgnoreCase("")) {
                    Toast.makeText(getApplicationContext(), "Please Enter new Password", Toast.LENGTH_SHORT).show();
                    return;

                }
                if (confirmPassword == "" || confirmPassword.equalsIgnoreCase("")) {
                    Toast.makeText(getApplicationContext(), "Please Enter confirm Password", Toast.LENGTH_SHORT).show();
                    return;

                }
                if (!confirmPassword.equalsIgnoreCase(newPassword)) {
                    Toast.makeText(getApplicationContext(), "Password does not match", Toast.LENGTH_SHORT).show();
                    return;

                }
                new AsyncTaskChange().execute();
            }
        });
    }


    private class AsyncTaskChange extends
            AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = new ProgressDialog(getContext());
            dialog.setMessage("Authenticating, Please wait...");
            dialog.setIndeterminate(false);
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.setProgress(0);
            int apiLevel = android.os.Build.VERSION.SDK_INT;
            if (apiLevel >= 11) {
                dialog.setProgressNumberFormat(null);
            }
            dialog.show();
        }


        @Override
        protected String doInBackground(Void... params) {
            // Log.d("email", email);
            String response = Services.getChangePassword(ChangePasswordActivity.this, userid, newPassword, "get_update_pass");
            System.out.println(response);
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            Log.d("onPostExecute ", response);
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            changePass(response);
        }
    }

    protected Context getContext() {
        return this;
    }

    public void changePass(String response) {
        if (response != null && !response.equals("")) {
            try {
                Log.d("krishna 1 ", response);
                JSONObject jsonobj = new JSONObject(response);
                if (jsonobj.has("ERROR")) {
                    Log.d("krishna 2 ", response);
                    if (jsonobj.getString("ERROR").equalsIgnoreCase("0")) {
                        Log.d("krishna 3 ", response);
                        if (jsonobj.has("DATA")) {
                            Log.d("krishna 4 ", response);
                            Intent sc = new Intent(ChangePasswordActivity.this, MainActivity.class);
                            startActivity(sc);
                            finish();
                        }

                    } else {
                        Toast t = Toast.makeText(ChangePasswordActivity.this, "No Products Found", Toast.LENGTH_SHORT);
                        t.show();
                    }
                } else {
                    Log.d("vimal 6", response);
                    Toast t = Toast.makeText(ChangePasswordActivity.this, "OOPS, Something went wrong!", Toast.LENGTH_SHORT);
                    t.show();
                }
            } catch (Exception e) {
                e.printStackTrace();

            }
        }
        if (dialog != null) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        }
    }
}
