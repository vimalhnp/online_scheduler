package com.example.sai.login_form;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;

public class DashboardActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener{

    AlertDialog.Builder alert;
    Button logout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        Button notice_btn=(Button) findViewById(R.id.notice_btn);
        notice_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ni = new Intent(DashboardActivity.this, NoticeActivity.class);
                startActivity(ni);
            }
        });

        Button feedback_btn=(Button) findViewById(R.id.feedback_btn);
        feedback_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent fi = new Intent(DashboardActivity.this, FeedbackActivity.class);
                startActivity(fi);
            }
        });

        Button app_btn=(Button) findViewById(R.id.appointment_btn);
        app_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ai = new Intent(DashboardActivity.this, AppointmentActivity.class);
                startActivity(ai);
            }
        });

        Button timetable_btn=(Button) findViewById(R.id.timetable_btn);
        timetable_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ti = new Intent(DashboardActivity.this, TimetableActivity.class);
                startActivity(ti);
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dashboard, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
//        switch (id) {
//
//            case R.id.logout:
//                new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
//                        .setTitleText("Are you sure?")
//                        .setContentText("You Want to Logout")
//                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                            @Override
//                            public void onClick(SweetAlertDialog sweetAlertDialog) {
//                                SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("login_data", MODE_PRIVATE);
//                                SharedPreferences.Editor e = sharedPreferences.edit();
//                                e.clear();
//                                e.commit();
//                                Intent intent = new Intent(dashboard.this, login.class);
//                                startActivity(intent);
//                            }
//                        })
//                        .show();
//                break;
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        switch (id){
            case R.id.profile:
                break;

            case R.id.changePassword:
                Intent ic = new Intent(DashboardActivity.this, ChangePasswordActivity.class);
                startActivity(ic);
                break;

            case R.id.about_us:
                Intent intent = new Intent(DashboardActivity.this, AboutUsActivity.class);
                startActivity(intent);
                break;

            case R.id.logout:
                SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("user_details", 0);
                SharedPreferences.Editor e = sharedPreferences.edit();
                e.clear();
                e.commit();
                Intent ia = new Intent(DashboardActivity.this, MainActivity.class);
                startActivity(ia);
                break;
        }

//        if (id == R.id.profile) {
//            // Handle the camera action
//        } else if (id == R.id.setting) {
//
//        } else if (id == R.id.about_us) {
//
//        } else if  (id == R.id.logout){
//
//            SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("user_details", 0);
//            SharedPreferences.Editor e = sharedPreferences.edit();
//            e.clear();
//            e.commit();
//            Intent intent = new Intent(DashboardActivity.this, MainActivity.class);
//            startActivity(intent);
//        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}