package com.example.sai.login_form;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sai.login_form.services.Services;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by sai on 1/26/2016.
 */
public class FeedbackActivity extends Activity {
    private ProgressDialog dialog;
    public ListView listview;
    public String[] title, content;
    int loader = R.drawable.home;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);

//        (WindowManager.LayoutParams.FLAG_FULLSCREEN), WindowManager.LayoutParams.FLAG_FULLSCREEN);


        setContentView(R.layout.activity_feedback_table);
        new AsyncTaskNotice().execute();


    }

    private class MySimpleArrayAdapter extends ArrayAdapter<String> {
        private final Context context;
        private String[] title = new String[80];
//        private final String[] message;

        public MySimpleArrayAdapter(Context context, String[] title) {
            super(context, R.layout.feedback_list, title);
            this.context = context;
            this.title = title;
//            this.message = message;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.feedback_list, parent, false);
            TextView textView = (TextView) rowView.findViewById(R.id.feedback_label);
//            ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
            Log.d("manisha 123", title[position]);
            if (title[position] == null) {
                // do nothing
//                Log.d("error", "error1");
            } else {
                textView.setText(title[position]);

//                ImageLoader imgLoader = new ImageLoader(getApplicationContext());
//                imgLoader.DisplayImage(message[position], loader, imageView);

//                imageView.setImageResource(R.drawable.result);
            }
            return rowView;
        }
    }

    private class AsyncTaskNotice extends
            AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = new ProgressDialog(getContext());
            dialog.setMessage("Authenticating, Please wait...");
            dialog.setIndeterminate(false);
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.setProgress(0);
            int apiLevel = android.os.Build.VERSION.SDK_INT;
            if (apiLevel >= 11) {
                dialog.setProgressNumberFormat(null);
            }
            dialog.show();
        }


        @Override
        protected String doInBackground(Void... params) {
            String response = Services.getProducts(FeedbackActivity.this, "get_feedback_list");

            System.out.println(response);

            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            Log.d("onPostExecute ", response);
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            fillProducts(response);
        }
    }

    protected Context getContext() {
        return this;
    }

    public void fillProducts(String response) {
        if (response != null && !response.equals("")) {
            try {
                Log.d("manisha 1 ", response);
                JSONObject jsonobj = new JSONObject(response);
                if (jsonobj.has("ERROR")) {
                    Log.d("manisha 2 ", response);
                    if (jsonobj.getString("ERROR").equalsIgnoreCase("0")) {
                        Log.d("manisha 3 ", response);
                        if (jsonobj.has("DATA")) {
                            Log.d("manisha 4 ", response);
                            JSONArray myProducts = jsonobj.getJSONArray("DATA");

                            title = new String[myProducts.length()];
                            content = new String[myProducts.length()];
                            for (int i = 0; i < myProducts.length(); i++) {
                                JSONObject tempjo = new JSONObject();
                                tempjo = myProducts.getJSONObject(i);
                                title[i] = tempjo.getString("title");
                                content[i] = tempjo.getString("content");
                            }

                            listview = (ListView) findViewById(R.id.feedback_listview);

                            final MySimpleArrayAdapter adapter = new MySimpleArrayAdapter(FeedbackActivity.this, title);
                            listview.setAdapter(adapter);

                            listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                                @Override
                                public void onItemClick(AdapterView<?> parent, final View view,
                                                        int position, long id) {
                                    // this is when we click on item of any list view
                                    SharedPreferences sharedPrefs = getContext().getSharedPreferences("feedback_data", 0);
                                    SharedPreferences.Editor e = sharedPrefs.edit();
                                    e.putString("f_title", title[position]);
                                    e.putString("content", content[position]);
                                    e.commit();
                                    Intent feed_detail = new Intent(FeedbackActivity.this, FeedbackDetailsActivity.class);
                                    startActivity(feed_detail);
                                }

                            });
                        }
                    } else {
                        Toast t = Toast.makeText(FeedbackActivity.this, "No Products Found", Toast.LENGTH_SHORT);
                        t.show();
                    }
                } else {
                    Log.d("manisha 6", response);
                    Toast t = Toast.makeText(FeedbackActivity.this, "OOPS, Something went wrong!", Toast.LENGTH_SHORT);
                    t.show();
                }

            } catch (Exception e) {
                e.printStackTrace();

            }

        }
        if (dialog != null) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        }
    }
}