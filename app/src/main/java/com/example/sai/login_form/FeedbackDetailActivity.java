package com.example.sai.login_form;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.melnykov.fab.FloatingActionButton;

/**
 * Created by SAI on 2/7/2016.
 */
public class FeedbackDetailActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences sharedPrefs = getContext().getSharedPreferences("feedback_data", 0);

        String detail_f_title = sharedPrefs.getString("f_title", "");
        String detail_content = sharedPrefs.getString("content", "");
        String detail_type = sharedPrefs.getString("type", "");
        String detail_from_whom = sharedPrefs.getString("from_whom", "");
        String detail_to_whom = sharedPrefs.getString("to_whom", "");

        //Remove title bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_feedback_details);

        TextView f_title=(TextView)findViewById(R.id.toolbar_layout);
        f_title.setText(detail_f_title);

        TextView content=(TextView)findViewById(R.id.content_txt);
        content.setText(detail_content);

        TextView type=(TextView)findViewById(R.id.type_txt);
        type.setText(detail_type);

        TextView from_whom=(TextView)findViewById(R.id.from_whom_txt);
        from_whom.setText(detail_from_whom);

        TextView to_whom=(TextView)findViewById(R.id.to_whom_txt);
        to_whom.setText(detail_to_whom);

//
//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                NoticeDetailActivity.this.finish();
//            }
//        });
    }
    protected Context getContext() {
        return this;
    }
}
