package com.example.sai.login_form;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

public class FeedbackDetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences sharedPrefs = this.getSharedPreferences("feedback_data", 0);

        String detail_f_title = sharedPrefs.getString("f_title", "");
        String detail_content = sharedPrefs.getString("content", "");
//        String detail_type = sharedPrefs.getString("type", "");
//        String detail_from_whom = sharedPrefs.getString("from_whom", "");
//        String detail_to_whom = sharedPrefs.getString("to_whom", "");

        setContentView(R.layout.activity_feedback_details);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        CollapsingToolbarLayout toolBarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        toolBarLayout.setTitle(sharedPrefs.getString("f_title",""));

        TextView d_content=(TextView)findViewById(R.id.content_text);
        d_content.setText(sharedPrefs.getString("content",""));

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.feedback_fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }
}
