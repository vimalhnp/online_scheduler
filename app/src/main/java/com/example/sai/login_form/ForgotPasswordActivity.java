package com.example.sai.login_form;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.*;

import com.example.sai.login_form.services.Services;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by sai on 05/22/2016.
 */
public class ForgotPasswordActivity extends Activity {
    private ProgressDialog dialog;

    public String[] userid;
    String email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        final EditText emltxt = (EditText) findViewById(R.id.email_txt);


        Button saveChangesBtn = (Button) findViewById(R.id.requestForChangePass);
        saveChangesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                email = emltxt.getText().toString();
                if(email == "" || email.equalsIgnoreCase("")){

                    Toast.makeText(getApplicationContext(),"Please Enter Registered EmailID",Toast.LENGTH_SHORT).show();
                    return;
                }
                new AsyncTaskForgot().execute();
            }
        });


    }

    private class AsyncTaskForgot extends
            AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = new ProgressDialog(getContext());
            dialog.setMessage("Authenticating, Please wait...");
            dialog.setIndeterminate(false);
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.setProgress(0);
            int apiLevel = android.os.Build.VERSION.SDK_INT;
            if (apiLevel >= 11) {
                dialog.setProgressNumberFormat(null);
            }
            dialog.show();
        }


        @Override
        protected String doInBackground(Void... params) {
            Log.d("email", email);
            String response = Services.getChangePass(ForgotPasswordActivity.this, email, "get_change_pass");
            System.out.println(response);
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            Log.d("onPostExecute ", response);
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            checkUserId(response);
        }
    }

    protected Context getContext() {
        return this;
    }

    public void checkUserId(String response) {
        if (response != null && !response.equals("")) {
            try {
                JSONObject jsonobj = new JSONObject(response);
                if (jsonobj.has("ERROR")) {
                    Log.d("krishna 2 ", response);
                    if (jsonobj.getString("ERROR").equalsIgnoreCase("0")) {
                        Log.d("krishna 3 ", response);
                        if (jsonobj.has("DATA")) {
                            Log.d("krishna 4 ", response);
                            JSONArray myProducts = jsonobj.getJSONArray("DATA");
                            userid = new String[myProducts.length()];
                            for (int i = 0; i < myProducts.length(); i++) {
                                JSONObject tempjo = new JSONObject();
                                tempjo = myProducts.getJSONObject(i);

                                SharedPreferences sharedPrefs = getContext().getSharedPreferences("changepass_details", 0);
                                SharedPreferences.Editor e = sharedPrefs.edit();
                                e.putString("user_id", tempjo.getString("user_id"));
                                e.commit();
                                Log.d("krishna 5",tempjo.getString("user_id"));

                            }

                            Intent cp = new Intent(ForgotPasswordActivity.this, ChangePasswordActivity.class);
                            startActivity(cp);
                        }
                    } else {
                        Toast t = Toast.makeText(ForgotPasswordActivity.this, "Your email id not registered!", Toast.LENGTH_SHORT);
                        t.show();
                    }
                } else {
                    Log.d("vimal 6", response);
                    Toast t = Toast.makeText(ForgotPasswordActivity.this, "OOPS, Something went wrong!", Toast.LENGTH_SHORT);
                    t.show();
                }
            } catch (Exception e) {
                e.printStackTrace();

            }
        }
        if (dialog != null) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        }
    }

}
