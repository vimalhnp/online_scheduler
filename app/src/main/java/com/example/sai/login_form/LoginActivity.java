package com.example.sai.login_form;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Created by sai on 10/18/2015.
 */
public class LoginActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Button notice_btn=(Button) findViewById(R.id.btn_notice);
        notice_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ni = new Intent(LoginActivity.this, NoticeActivity.class);
                startActivity(ni);
            }
        });

        Button feedback_btn=(Button) findViewById(R.id.btn_feedback);
        feedback_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent fi = new Intent(LoginActivity.this, FeedbackActivity.class);
                startActivity(fi);
            }
        });

        Button app_btn=(Button) findViewById(R.id.btn_appointment);
        app_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ai = new Intent(LoginActivity.this, AppointmentActivity.class);
                startActivity(ai);
            }
        });

        Button timetable_btn=(Button) findViewById(R.id.btn_timetable);
        timetable_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ti = new Intent(LoginActivity.this, TimetableActivity.class);
                startActivity(ti);
            }
        });
    }
    }