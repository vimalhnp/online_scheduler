package com.example.sai.login_form;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
//import android.support.v7.app.ActionBarActivity;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.sai.login_form.services.Services;

import org.json.JSONObject;

import static android.view.View.*;


public class MainActivity extends Activity {

    String userName,Password;
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText uName = (EditText) findViewById(R.id.username);
        final EditText pass = (EditText) findViewById(R.id.password);

        SharedPreferences sharedPrefs = getContext().getSharedPreferences("user_details", 0);
        String userid = sharedPrefs.getString("user_id", "");

        if(!userid.equalsIgnoreCase("") )
        {
            Intent dash = new Intent(MainActivity.this,DashboardActivity.class);
            startActivity(dash);
            finish();

            Toast w = Toast.makeText(MainActivity.this, "Welcome back", Toast.LENGTH_SHORT);
            w.show();
        }

        final Button b = (Button) findViewById(R.id.btn_login);
        b.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                userName=uName.getText().toString();
                Password=pass.getText().toString();

                if(userName == "" || userName.equalsIgnoreCase("")){

                    Toast.makeText(getApplicationContext(),"Please Complete All Fields",Toast.LENGTH_SHORT).show();
                    return;
                }
                else if (Password == "" || Password.equalsIgnoreCase("")){

                    Toast.makeText(getApplicationContext(),"Please Complete All Fields",Toast.LENGTH_SHORT).show();
                    return;
                }
                else if (userName != "" || Password != ""){
                    new AsyncTaskLogin().execute();

                    Toast.makeText(getApplicationContext(),"Login Successfully", Toast.LENGTH_SHORT).show();
                }
                new AsyncTaskLogin().execute();
            }


        });

        Button btn = (Button) findViewById(R.id.forgotPass);
        btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent fi=new Intent(MainActivity.this, ForgotPasswordActivity.class);
                startActivity(fi);
            }
        });
    }

    private class AsyncTaskLogin extends
            AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.d("vimal log", userName);

            dialog = new ProgressDialog(getContext());
            dialog.setMessage("Please wait...");
            dialog.setIndeterminate(false);
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.setProgress(0);
            int apiLevel = android.os.Build.VERSION.SDK_INT;
            if (apiLevel >= 11) {
                dialog.setProgressNumberFormat(null);
            }
            dialog.show();

//            if (!(Utilsa.isInternetConnected(getContext()))) {
//                System.out.println("No Internet Connection.....");
//                Toast.makeText(getContext(), "Internet Connection is required",
//                        Toast.LENGTH_SHORT).show();
//                return;
//            }
        }
        @Override
        protected String doInBackground(Void... params) {
            String response = Services.doLogin(MainActivity.this, "login", userName, Password);

//Log.d("arpit",response);
            System.out.println(response);

            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            Log.d("onPostExecute ", response);
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            //HomeDeliveryUtils.dimissProgress();
            parseLoginResponse(response);
        }
    }
    protected Context getContext() {
        return this;
    }

    public void parseLoginResponse(String response) {
        if (response != null && !response.equals("")) {
            try {
                JSONObject jsonobj = new JSONObject(response);
                if (jsonobj.has("ERROR")) {
                    Log.d("vimal 3", response);
                    if (jsonobj.getString("ERROR").equalsIgnoreCase("0")) {
                        Log.d("vimal 12", response);
                        if (jsonobj.has("DATA")) {
                            Log.d("vimal 4", response);

                            String user_id = jsonobj.getString("user_id");
                            SharedPreferences sharedPrefs = getContext().getSharedPreferences("user_details", 0);
                            SharedPreferences.Editor e = sharedPrefs.edit();
                            e.putString("user_id", user_id);
                            e.commit();
//                            if (Integer.parseInt(user_id) > 0) {
                            Intent HomeIntent = new Intent(MainActivity.this, DashboardActivity.class);
                            startActivity(HomeIntent);
                            finish();
//                            } else {
//                                Toast t = Toast.makeText(LoginActivity.this, "Username Password invalid", Toast.LENGTH_SHORT);
//                                t.show();
//                            }
                        }
                    } else {
                        Toast t = Toast.makeText(MainActivity.this, "Username Password invalid", Toast.LENGTH_LONG);
                        t.show();
                    }
                } else {
                    Log.d("vimal 5", response);
                    Toast t = Toast.makeText(MainActivity.this, "Username Password invalid", Toast.LENGTH_SHORT);
                    t.show();
                }
            } catch (Exception e) {
                e.printStackTrace();

            }
        }
        if (dialog != null) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}