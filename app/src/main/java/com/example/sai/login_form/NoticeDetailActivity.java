package com.example.sai.login_form;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.melnykov.fab.FloatingActionButton;

/**
 * Created by SAI on 2/7/2016.
 */
public class NoticeDetailActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences sharedPrefs = getContext().getSharedPreferences("notice_data", 0);

        String detail_title = sharedPrefs.getString("title", "");
        String detail_msg = sharedPrefs.getString("message", "");
        String detail_freq = sharedPrefs.getString("frequency", "");
        String detail_start_date = sharedPrefs.getString("start_date", "");
        String detail_end_date = sharedPrefs.getString("end_date", "");

        //Remove title bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_notice_detail);

        TextView title=(TextView)findViewById(R.id.title_txt);
        title.setText(detail_title);

        TextView msg=(TextView)findViewById(R.id.msg_txt);
        msg.setText(detail_msg);

        TextView frequency=(TextView)findViewById(R.id.freq_txt);
        frequency.setText(detail_freq);

        TextView s_date=(TextView)findViewById(R.id.start_date_txt);
        s_date.setText(detail_start_date);

        TextView e_date=(TextView)findViewById(R.id.end_date_txt);
        e_date.setText(detail_end_date);

//
//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                NoticeDetailActivity.this.finish();
//            }
//        });
    }
    protected Context getContext() {
        return this;
    }
}
