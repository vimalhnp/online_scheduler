package com.example.sai.login_form;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

public class NoticeDetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences sharedPrefs = this.getSharedPreferences("notice_data", 0);
        String detail_title = sharedPrefs.getString("title", "");
        String detail_msg = sharedPrefs.getString("message", "");
        String detail_freq = sharedPrefs.getString("frequency", "");
        String detail_start_date = sharedPrefs.getString("start_date", "");
        String detail_end_date = sharedPrefs.getString("end_date", "");

        setContentView(R.layout.activity_notice_details);

        Toolbar notice_Detail = (Toolbar) findViewById(R.id.notice_detail);
        setSupportActionBar(notice_Detail);

        CollapsingToolbarLayout noticeLayout = (CollapsingToolbarLayout) findViewById(R.id.notice_detail_layout);
        noticeLayout.setTitle(sharedPrefs.getString("title",""));

        TextView Notice_detail=(TextView)findViewById(R.id.notice_text);
        Notice_detail.setText(sharedPrefs.getString("message",""));

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.notice_fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }
}
