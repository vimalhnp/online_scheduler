package com.example.sai.login_form;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sai.login_form.services.Services;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by sai on 1/26/2016.
 */
public class TimetableActivity extends Activity {
    private ProgressDialog dialog;
    public String[] lecture_time, subject, class_standard;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;
//    int loader = R.drawable.home;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);

//        (WindowManager.LayoutParams.FLAG_FULLSCREEN), WindowManager.LayoutParams.FLAG_FULLSCREEN);


        setContentView(R.layout.activity_time_table);
        new AsyncTaskNotice().execute();


        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Timetable Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.example.sai.login_form/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Timetable Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.example.sai.login_form/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }
//    private class MySimpleArrayAdapter extends ArrayAdapter<String> {
//        private final Context context;
//        private String[] title = new String[0];
////        private final String[] message;
//
//        public MySimpleArrayAdapter(Context context, String[] title) {
//            super(context, R.layout.notification_list, title);
//            this.context = context;
//            this.title = title;
////            this.message = message;
//        }
//
//        @Override
//        public View getView(int position, View convertView, ViewGroup parent) {
//            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            View rowView = inflater.inflate(R.layout.notification_list, parent, false);
//            TextView textView = (TextView) rowView.findViewById(R.id.label);
////            ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
//            if (title[position] == null || message[position] == null) {
//                // do nothing
////                Log.d("error", "error1");
//            } else {
//                textView.setText(title[position]);
//
////                ImageLoader imgLoader = new ImageLoader(getApplicationContext());
////                imgLoader.DisplayImage(message[position], loader, imageView);
//
////                imageView.setImageResource(R.drawable.result);
//            }
//            return rowView;
//        }
//    }

    private class AsyncTaskNotice extends
            AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            dialog = new ProgressDialog(getContext());
            dialog.setMessage("Authenticating, Please wait...");
            dialog.setIndeterminate(false);
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.setProgress(0);
            int apiLevel = Build.VERSION.SDK_INT;
            if (apiLevel >= 11) {
                dialog.setProgressNumberFormat(null);
            }
            dialog.show();
        }


        @Override
        protected String doInBackground(Void... params) {
            SharedPreferences sharedPrefs = getContext().getSharedPreferences("user_details", 0);
            String userid = sharedPrefs.getString("user_id", "");


            String response = Services.getTimetable(TimetableActivity.this, "get_timetable", userid);


            System.out.println(response);

            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            Log.d("onPostExecute ", response);
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            fillProducts(response);
        }
    }

    protected Context getContext() {
        return this;
    }

    public void fillProducts(String response) {
        if (response != null && !response.equals("")) {
            try {
                Log.d("xyz 1 ", response);
                JSONObject jsonobj = new JSONObject(response);
                if (jsonobj.has("ERROR")) {
                    Log.d("xyz 2 ", response);
                    if (jsonobj.getString("ERROR").equalsIgnoreCase("0")) {
                        Log.d("xyz 3 ", response);
                        if (jsonobj.has("DATA")) {
                            Log.d("xyz 4 ", response);
                            JSONArray myProducts = jsonobj.getJSONArray("DATA");

                            lecture_time = new String[myProducts.length()];
                            subject = new String[myProducts.length()];
                            class_standard = new String[myProducts.length()];
                            for (int i = 0; i < myProducts.length(); i++) {
                                String column = "1";
                                JSONObject tempjo = new JSONObject();
                                tempjo = myProducts.getJSONObject(i);
                                Log.d("vimal123 ",tempjo.getString("lecture_day"));

                                if (tempjo.getString("lecture_day").equalsIgnoreCase("Monday")) {
                                    column = "1";
                                }
                                else if (tempjo.getString("lecture_day").equalsIgnoreCase("Tuesday")) {
                                    column = "2";
                                }
                                else if (tempjo.getString("lecture_day") .equalsIgnoreCase("Wednesday")) {
                                    column = "3";
                                }
                                else if (tempjo.getString("lecture_day")  .equalsIgnoreCase("Thursday")) {
                                    column = "4";
                                }
                                else if (tempjo.getString("lecture_day")  .equalsIgnoreCase("Friday")) {
                                    column = "5";
                                }
                                else if (tempjo.getString("lecture_day")  .equalsIgnoreCase("Saturday")) {
                                    column = "6";
                                }
//                                subject[i] = tempjo.getString("subject");
//                                class_standard[i] = tempjo.getString("class_standard");

                                String subID = "sub" + column+"_"+i+"_txt";
                                Log.d("vimal123 123",subID);

                                int resID = getResources().getIdentifier(subID, "id", getPackageName());

                                TextView tv = (TextView) findViewById(resID);
                                tv.setText(tempjo.getString("subject"));

                            }
                            Log.d("xyz 11", "hi");
//                            n_listview = (ListView) findViewById(R.id.notice_listview);
//
//                            final MySimpleArrayAdapter adapter = new MySimpleArrayAdapter(NoticeActivity.this, title);
//                            n_listview.setAdapter(adapter);
//
//                            n_listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//
//                                @Override
//                                public void onItemClick(AdapterView<?> parent, final View view,
//                                                        int position, long id) {
//                                    // this is when we click on item of any list view
//                                    SharedPreferences sharedPrefs = getContext().getSharedPreferences("notice_data", 0);
//                                    SharedPreferences.Editor e = sharedPrefs.edit();
//                                    e.putString("title",title[position]);
//                                    e.putString("message",message[position]);
//                                    e.putString("frequency", frequency[position]);
//                                    e.commit();
//                                    Intent notice_detail = new Intent(NoticeActivity.this, NoticeDetailsActivity.class);
//                                    startActivity(notice_detail);
//                                }
//
//                            });
                        }
                    } else {
                        Toast t = Toast.makeText(TimetableActivity.this, "Not Recognized", Toast.LENGTH_SHORT);
                        t.show();
                    }
                } else {
                    Log.d("vimal 6", response);
                    Toast t = Toast.makeText(TimetableActivity.this, "OOPS, Something went wrong!", Toast.LENGTH_SHORT);
                    t.show();
                }
            } catch (Exception e) {
                e.printStackTrace();

            }
        }
        if (dialog != null) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        }
    }
}