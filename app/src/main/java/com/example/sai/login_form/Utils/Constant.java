package com.example.sai.login_form.Utils;

public class Constant 
{


	public static final String PREF_LOGIN = "login_prefs";
	public static final String INITIAL_DETAILS = "initialDetails";
	public static String CHECK_FLAG = "check_flag";
	public static final String FACEBOOK_STATES = "facebook_state";
	public static String EXTRA_URL = "extra_url";
	public  static final String USER_DATAILS = "user_details";
	public static final String TEMP_PHOTO_FILE_NAME = "temp_photo.jpg";
	public static final String CLIENT_ID = "ef1f5d75bb474bfea7fb7b919370e0ff";
	public static final String CLIENT_SECRET = "06f0c454c6fe4bfc870e1673a7cfd72b";
	public static final String CALLBACK_URL = "igef1f5d75bb474bfea7fb7b919370e0ff://authorize";
	public static final String ACCESSCODE_FLAG = "accessCode";
	public static boolean onCreateLoaded = false;
	
	
	
	
	// public static final String RESPONSE = "fbresponse";
	//public static final String URL_PREFIX = "http://dmbdemo.com/bll/source/api/index.php";
	// public static final String URL_PREFIX =
	// "http://dmbdemo.com/new-unoapp/source/api/index.php";
	public static final String URL_PREFIX = "http://unoapp.com/app/source/api/index.php";

	public static final String API_ID = "api_id";
	public static final String API_SECRET = "api_secret";
	public static final String API_ID_VALUE = "bll_service";
	public static final String API_SECRET_VALUE = "77757c129a9f42a28c0ae44d329361ca";
	public static final String LOADING_MESSAGE = "Loading up #BudLightLiving";

	public static final String COMPANY_ID_VALUE = "1";
	public static String COMPANY_ID = "companyId";
	public static String BUDLIGHTLIVING = "#BudLightLiving";

	// public static String APP_ID = "728615400528729";//chextest
	public static String APP_ID = "543751665769070";// BudLightLIving

	public static final String MESSAGE = "message";
	public static String PHOTOS = "photos";
	public static String POST = "post";
	public static String BUD_LIGHT_LIVING = "Bud Light Living";
	public static final String LATITUDE = "latitude";
	public static String LONGITUDE = "longitude";
	public static String FLAG = "flag";
	public static String DATA = "data";
	public static String RESPONSE = "response";
	public static String ROTATION_RESPONSE = "rotation_response";

	public static String RESULT = "result";
	public static String SUCCESS = "success";

	public static String IS_LOGIN = "is_login";

	public static String TERMS_OF_USE = "By tapping to continue, you are indicating that you \n have read the Privacy Policy, and agree to \n Terms & Services and Rules and Regulations.";

	public static String USERID = "userId";
	public static String END_TIME = "end_time";
	public static String USERNAME = "userName";
	public static String FACEBOOKID = "facebookId";
	public static String FACEBOOK_ACCESS_TOKEN = "facebook_access_token";
	public static String EMAIL = "email";
	public static String PROIVINCE_ID = "provinceId";
	public static String BIRTHDATE = "birthdate";
	public static String DEVICE_ID = "device_id";
	public static String PHONE_MODEL = "phone_model";
	public static String OS_VERSION = "os_version";
	public static String APPVERSION = "app_version";
	public static String DEVICE_TYPE = "device_type";
	public static String COUNTRY_CODE = "country_code";
	public static String GMT_OFFSET = "gmt_offset";
	public static String TIMEZONE = "timezone";
	public static String CARRIER_NAME = "carrier_name";
	public static String CURRENT_LOCATION_GPS = "current_location_gps";
	public static String CURRENT_LOCATION_FACEBOOK = "current_location_facebook";
	public static String PHOTO_URL = "photoUrl";
	public static String LOCATION = "location";
	public static String GPS_LOCATION = "gps_location";
	public static String FACEBOOK_ID = "facebookId";
	public static String FACEBOOK_AGE = "facebookAge";
	public static String MEDIA_ID = "mediaId";
	public static String MEDIA_URL = "mediaUrl";
	public static String STATUS = "extra_status";
	public static String BLL_METER = "bllMeter";
	public static String BLL_STATUS = "status";

	public static String API_TYPE = "api_type";
	public static String APP_TYPE = "app_type";
	public static String API_TYPE_FOR_WHAT = "api_type_for_what";
	public static String LIVE = "live";
	public static String TESTING = "testing";
	public static String BLLLEVEL = "bllLevel";

	public static String EVENT_ID = "eventId";
	public static String ACCESS_CODE = "access_code";

	public static String ALBUM_ID = "id";
	public static String LOCAL_ALBUM_DIR_PATH = "localAlbumDirectoryPath";
	public static String ALBUM_FLAG = "null";
	
	public static String DASHBOARD_FLAG = "dashboard_flag";

	public static String FOLDER_BLL = "BLL";
	public static String FOLDER_PROFILE_PICTURE = "Profile Pictures";
	public static String FOLDER_MEDIA = "Media";
	public static String NO_MEDIA_FILE = ".nomedia";

	public static String INSTAGRAM_ID = "id";
	public static String INSTAGRAM_FULL_NAME = "full_name";
	public static String INSTAGRAM_ACCESS_TOKAN = "instagram_access_token";
	public static String INSTAGRAM_USER_NAME = "username";
	public static String INSTAGRAM_ATTRIBUTE = "itattribute";
	public static String SHARED_PHOTO_URL = "imageurl";
	public static String SHARED_VIDEO_URL = "videoUrl";
	public static String SHARED_PHOTO_THUMBNAIL = "thumbnail";

	public static String PAST_WINNER_ID = "userId";
	public static String TOAST_MESSAGE_EXIT_MESSAGE = "Press again to exit";

	public static final String SERVER_URL = "http://www.moodlytics.com/daxesh/register.php";
	// Google project id
	// static final String SENDER_ID = "318910472534";
	public static final String SENDER_ID = "886912565648";
	public static final String TAG = "iSeeiAct GCM";
	public static final String DISPLAY_MESSAGE_ACTION = "com.anantapps.iseeiact.DISPLAY_MESSAGE";
	public static final String EXTRA_MESSAGE = "message";
	public static final String COUNTDOWN_ENDINGS = "countdown_endings";
	public static final String NEW_EVENT_ANNOUNCEMENT = "new_event_announcement";

	public static final String PUSH_NOTIFICATION_MESSAGE = "PushNotifcationMessage";
	public static final String PUSH_NOTIFICATION_USER_ID = "PushNotifcationUserId";
	public static final String PUSH_NOTIFICATION_EVENT_ID = "PushNotifcationEventId";
	public static final String PUSH_NOTIFICATION_MESSAGE_SEPRATOR = "PushNotifcationMessageSeprator";

	public static String IS_FROM_PUSH_NOTIFICATION = "IS_FROM_PUSH_NOTIFICATION";
	public static String NOTIFICATION_TYPE = "type";
	public static String NOTIFICATION_TYPE_COUNTDOWN_ENDS = "1";
	public static String NOTIFICATION_TYPE_COUNTDOWN_START = "2";
	public static String NOTIFICATION_TYPE_WINNER_ANNOUNCE = "3";
	public static String NOTIFICATION_TYPE_WINNER_ANNOUNCE_FOR_OTHER_USERS = "4";
	public static String IN_APP_PNOTIFICATION_TYPE = "7";
	public static String SHARED_PREF_SETTINGS = "shared_pref_settings";

	public static String LAST_SEEN_TIME = "lastSeenTime";
	public static String WINNER_NOT_ANNOUNCED = "winner not announced";

}
