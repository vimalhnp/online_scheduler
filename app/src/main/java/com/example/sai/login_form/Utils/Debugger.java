package com.example.sai.login_form.Utils;

import android.util.Log;

public class Debugger {

	final static String TAG="Bud Light Living";
	
	public static void logE(String message) {
		Log.e(TAG, message);
	}
	
	public static void logE(String tag,String message) {
		Log.e(TAG,tag+" --> "+ message);
	}
	
	public static void logD(String message) {
		Log.d(TAG, message);
	}
	
	public static void logD(String tag,String message) {
		Log.d(TAG,tag+" --> "+ message);
	}
	
	public static void logI(String message) {
		Log.i(TAG, message);
	}
	
	public static void logI(String tag,String message) {
		Log.i(TAG,tag+" --> "+ message);
	}
}
