package com.example.sai.login_form.Utils;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application.ActivityLifecycleCallbacks;
import android.os.Bundle;

@TargetApi(14)
public class MyLifecycleHandler implements ActivityLifecycleCallbacks {
    // I use four separate variables here. You can, of course, just use two and
    // increment/decrement them instead of using four and incrementing them all.
//    private int resumed;
//    private int paused;
//    private int started;
//    private int stopped;

 // Replace the four variables above with these four
    private static int resumed;
    private static int paused;
    private static int started;
    private static int stopped;
    
    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
       // android.util.Log.w("test", "onActivityCreated "+activity.getClass().getSimpleName());

    }

    @Override
    public void onActivityDestroyed(Activity activity) {
   //  android.util.Log.w("test", "onActivityDestroyed "+activity.getClass().getSimpleName());
    }

    @Override
    public void onActivityResumed(Activity activity) {
//     android.util.Log.w("test", "onActivityResumed "+activity.getClass().getSimpleName());
      ++resumed;
//        android.util.Log.w("test", "resumed "+resumed);
//        android.util.Log.w("test", "paused "+paused);
//        android.util.Log.w("test", "started "+started);
//        android.util.Log.w("test", "stopped "+stopped);
    }

    @Override
    public void onActivityPaused(Activity activity) {
//     android.util.Log.w("test", "onActivityPaused "+activity.getClass().getSimpleName());
    	++paused;
//        android.util.Log.w("test", "resumed "+resumed);
//        android.util.Log.w("test", "paused "+paused);
//        android.util.Log.w("test", "started "+started);
//        android.util.Log.w("test", "stopped "+stopped);
     // android.util.Log.w("test", "application is in foreground: " + (resumed > paused));
        
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
     //android.util.Log.w("test", "onActivitySaveInstanceState "+activity.getClass().getSimpleName());
    }

    @Override
    public void onActivityStarted(Activity activity) {
//     android.util.Log.w("test", "onActivityStarted "+activity.getClass().getSimpleName());
       ++started;
//        android.util.Log.w("test", "resumed "+resumed);
//        android.util.Log.w("test", "paused "+paused);
//        android.util.Log.w("test", "started "+started);
//        android.util.Log.w("test", "stopped "+stopped);
    }

    @Override
    public void onActivityStopped(Activity activity) {
//     android.util.Log.w("test", "onActivityStopped "+activity.getClass().getSimpleName());
        ++stopped;
//        android.util.Log.w("test", "resumed "+resumed);
//        android.util.Log.w("test", "paused "+paused);
//        android.util.Log.w("test", "started "+started);
//        android.util.Log.w("test", "stopped "+stopped);
       // android.util.Log.w("test", "application is visible: " + (started > stopped));
    }

    // If you want a static function you can use to check if your application is
    // foreground/background, you can use the following:

    // And these two public static functions
    public static boolean isApplicationVisible() {
//     android.util.Log.w("test", "resumed "+resumed);
//        android.util.Log.w("test", "paused "+paused);
//        android.util.Log.w("test", "started "+started);
//        android.util.Log.w("test", "stopped "+stopped);
        return started > stopped;
    }

    public static boolean isApplicationInForeground() {
     //android.util.Log.w("test", "resumed "+resumed);
       // android.util.Log.w("test", "paused "+paused);
        //android.util.Log.w("test", "started "+started);
        //android.util.Log.w("test", "stopped "+stopped);
        return resumed > paused;
    }
    
}