package com.example.sai.login_form.Utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class Utils {

	public static boolean isInternetConnected(Context mContext) {

		ConnectivityManager connec = (ConnectivityManager) mContext
				.getSystemService(Context.CONNECTIVITY_SERVICE);

		// ARE WE CONNECTED TO THE NET

		try {
			if (connec.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED
					|| connec.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED) {
				// Debugger.logE("connected");
				return true;
			} else {

			}
		} catch (Exception e) {
			// Debugger.logE("connect exception" + e.toString());

			e.printStackTrace();

			ConnectivityManager conMgr = (ConnectivityManager) mContext
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
			if (netInfo != null && netInfo.isConnectedOrConnecting())
				return true;
			else
				return false;

		}
		return false;

	}

	public static void showAlertDialog(Context context) {

		AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
		builder1.setMessage("It seems you are not connected to the internet. Kindly connect and try again");
		builder1.setTitle("Message");
		builder1.setCancelable(true);
		builder1.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();

			}
		});
		AlertDialog alert11 = builder1.create();
		alert11.show();
	}

}
