package com.example.sai.login_form.services;

import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.util.List;


public class RequestManager
{
	public static String callGet(String urlString)
	{

		Log.d("RequestManager","Url String : " + urlString);
		HttpParams params = new BasicHttpParams();
		HttpConnectionParams.setSoTimeout(params, 10000);
		HttpConnectionParams.setConnectionTimeout(params, 10000);
		HttpClient httpclient = new DefaultHttpClient(params);
		HttpGet httpget = new HttpGet(urlString);
		try
		{
			HttpResponse response = httpclient.execute(httpget);
			//Debugger.logE("Response  : " + response);
			return EntityUtils.toString(response.getEntity());
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			return "";
		}
	}

	public static String callPut(String urlString, List<NameValuePair> values) 
	{
		Log.d("RequestManager","Url String : " + urlString);
		HttpParams params = new BasicHttpParams();
		HttpConnectionParams.setSoTimeout(params, 100000);
		HttpConnectionParams.setConnectionTimeout(params, 100000);
		HttpClient httpclient = new DefaultHttpClient(params);
		HttpPut httppost = new HttpPut(urlString);

		try
		{
			httppost.setEntity(new UrlEncodedFormEntity(values));
			Log.d("RequestManager","Http Put "+ EntityUtils.toString(httppost.getEntity()));
			Log.d("RequestManager"," values "+ values.toString());
			HttpResponse response = httpclient.execute(httppost);
			return EntityUtils.toString(response.getEntity());
		}
		catch (Exception e)
		{
			return "";
		}
	}
	
	public static String callPostInSplash(String urlString, List<NameValuePair> values) 
	{
		Log.d("RequestManager","Url String : " + urlString);
		HttpParams params = new BasicHttpParams();
		HttpConnectionParams.setSoTimeout(params, 50000);
		HttpConnectionParams.setConnectionTimeout(params, 50000);
		HttpClient httpclient = new DefaultHttpClient(params);
		HttpPost httppost = new HttpPost(urlString);

		try
		{
			httppost.setEntity(new UrlEncodedFormEntity(values));
			Log.d("RequestManager","Http Post "+ EntityUtils.toString(httppost.getEntity()));
			Log.d("RequestManager"," values "+ values.toString());
			HttpResponse response = httpclient.execute(httppost);
			return EntityUtils.toString(response.getEntity());
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return "";
		}
	}
	
	public static String callPost(String urlString, List<NameValuePair> values) 
	{
		Log.d("RequestManager","Url String : " + urlString);
		HttpParams params = new BasicHttpParams();
		HttpConnectionParams.setSoTimeout(params, 100000);
		HttpConnectionParams.setConnectionTimeout(params, 100000);
		HttpClient httpclient = new DefaultHttpClient(params);
		HttpPost httppost = new HttpPost(urlString);

		try
		{
			httppost.setEntity(new UrlEncodedFormEntity(values));
			
//			httppost.setEntity(new ByteArrayEntity(
//				    values.toString().getBytes("UTF8")));
			Log.d("RequestManager","Http Post "+ EntityUtils.toString(httppost.getEntity()));
			Log.d("RequestManager"," values "+ values.toString());
			HttpResponse response = httpclient.execute(httppost);
			Log.d("response", "vim "+response.toString());
//			 String inputLine ;
//			 BufferedReader in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
//			 try {
//			       while ((inputLine = in.readLine()) != null) {
//			              System.out.println(inputLine);
//			       }
//			       in.close();
//			  } catch (IOException e) {
//			       e.printStackTrace();
//			  }
//			 
//			 return "Vimal";
			String entity = EntityUtils.toString(response.getEntity());
			Log.d("finalManager", entity);
			return entity;
			//return EntityUtils.toString(response.getEntity());
		}
		catch (Exception e)
		{
			Log.d("vim", "exception " +e);
			e.printStackTrace();
			return "";
		}
	}
	
	public static String callPost(String urlString, JSONObject values) 
	{
		Log.d("RequestManager","Url String : " + urlString);
		HttpParams params = new BasicHttpParams();
		HttpConnectionParams.setSoTimeout(params, 100000);
		HttpConnectionParams.setConnectionTimeout(params, 100000);
		HttpClient httpclient = new DefaultHttpClient(params);
		HttpPost httppost = new HttpPost(urlString);

		try
		{
			httppost.setEntity(new StringEntity(values.toString(), HTTP.UTF_8));
			
			//httppost.setEntity(new ByteArrayEntity(
				    //values.toString().getBytes("UTF8")));
			
			Log.d("RequestManager","Http Post "+ EntityUtils.toString(httppost.getEntity()));
			Log.d("RequestManager"," values "+ values.toString());
			HttpResponse response = httpclient.execute(httppost);
			Log.d("response", "dax "+response.toString());
//			 String inputLine ;
//			 BufferedReader in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
//			 try {
//			       while ((inputLine = in.readLine()) != null) {
//			              System.out.println(inputLine);
//			       }
//			       in.close();
//			  } catch (IOException e) {
//			       e.printStackTrace();
//			  }
//			 
//			 return "Daxesh";
			String entity = EntityUtils.toString(response.getEntity());
			Log.d("entity", entity);
			return entity;
			//return EntityUtils.toString(response.getEntity());
		}
		catch (Exception e)
		{
			Log.d("dax", "exception " +e);
			e.printStackTrace();
			return "";
		}
	}
	
	public static String callDelete(String urlString) 
	{
		Log.d("RequestManager","Url String : " + urlString);
		HttpParams params = new BasicHttpParams();
		HttpConnectionParams.setSoTimeout(params, 10000);
		HttpConnectionParams.setConnectionTimeout(params, 10000);
		HttpClient httpclient = new DefaultHttpClient(params);
		HttpDelete httpdelete = new HttpDelete(urlString);

		try 
		{
			Log.d("RequestManager","Http delete "+ httpdelete.getParams().toString());
			HttpResponse response = httpclient.execute(httpdelete);
			return EntityUtils.toString(response.getEntity());
		}
		catch (Exception e) 
		{
			return "";
		}
	}
}
