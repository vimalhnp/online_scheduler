package com.example.sai.login_form.services;

import android.content.Context;
import android.util.Log;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Services {

//	public static final String TAPI_ID = "$apiid";
//	public static final String TAPI_SECRET = "$api_secret";

    // Home Delivery Detail
    public static final String URL_PREFIX = "http://vdapplications.com/online_scheduler/apis/vimal_api.php/";
//    public static final String URL_PREFIX = "http://vahinfosol.com/apis/stock_market.php";

    public static String DATA = "data";
    // parameters
    public static final String FIRST_NAME = "username";
    public static final String EMAIL_ID = "email_id";
    public static final String PASSWORD = "password";
    public static final String PHONE = "contact_no";

    // services
    public static final String REG_ACTION = "action";
    // home_delivery/invitefriends

    // for signupUser
    public static String signUpUser(Context context, String username,
                                    String password, String email_id, String phone, String image, String action) {

        JSONObject mainData = new JSONObject();
        List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>(3);

        try {
//            JSONObject data = new JSONObject();
//            mainData.put(FIRST_NAME, username);
//            mainData.put(EMAIL_ID, email_id);
//            mainData.put(PASSWORD, password);
//            mainData.put(PHONE, phone);
//            mainData.put(DATA, data);
//            mainData.put("action", action);


            nameValuePair.add(new BasicNameValuePair(FIRST_NAME, username));
            nameValuePair.add(new BasicNameValuePair(PASSWORD, password));
            nameValuePair.add(new BasicNameValuePair(REG_ACTION, action));
            nameValuePair.add(new BasicNameValuePair(EMAIL_ID, email_id));
            nameValuePair.add(new BasicNameValuePair(PHONE, phone));
            nameValuePair.add(new BasicNameValuePair("profile_pic", image));

        } catch (Exception e) {
            e.printStackTrace();
        }

        String response = RequestManager.callPost(URL_PREFIX, nameValuePair);
        if (response != null && !response.equalsIgnoreCase("")) {
            return response;
        }

        return "";
    }

    // for doLogin
    public static String doLogin(Context context, String action, String userName, String Password) {

//		JSONObject mainData = new JSONObject();
        JSONObject data = new JSONObject();
        List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>(3);
        try {
            data.put("login", action);
            data.put("username", userName);
            data.put("password", Password);


            nameValuePair.add(new BasicNameValuePair("username", userName));
            nameValuePair.add(new BasicNameValuePair("password", Password));
            nameValuePair.add(new BasicNameValuePair("action", action));


//			mainData.put(DATA, data);
//			mainData.put(API_ID, API_ID_VALUE);
//			mainData.put(API_SECRET, API_SECRET_VALUE);

        } catch (Exception e) {
            e.printStackTrace();
        }

        String response = RequestManager.callPost(URL_PREFIX, nameValuePair);
        if (response != null && !response.equalsIgnoreCase("")) {
            return response;
        }

        return "";
    }

    public static String getProducts(Context context, String action) {
        List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>(3);
        try {
            nameValuePair.add(new BasicNameValuePair("action", action));
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.d("kris ",nameValuePair.toString());
        String response = RequestManager.callPost(URL_PREFIX, nameValuePair);
        if (response != null && !response.equalsIgnoreCase("")) {
            return response;
        }

        return "";
    }


    public static String getChangePass(Context context, String eml, String action) {
        List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>(3);
        try {
            nameValuePair.add(new BasicNameValuePair("action", action));
            nameValuePair.add(new BasicNameValuePair("email", eml));
        } catch (Exception e) {
            e.printStackTrace();
        }

        String response = RequestManager.callPost(URL_PREFIX, nameValuePair);
        if (response != null && !response.equalsIgnoreCase("")) {
            return response;
        }

        return "";
    }

    public static String getChangePassword(Context context,String userid, String newPass,String action) {
        List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>(3);
        try {
            nameValuePair.add(new BasicNameValuePair("action", action));
            nameValuePair.add(new BasicNameValuePair("user_id", userid));
            nameValuePair.add(new BasicNameValuePair("password", newPass));

        } catch (Exception e) {
            e.printStackTrace();
        }

        String response = RequestManager.callPost(URL_PREFIX, nameValuePair);
        if (response != null && !response.equalsIgnoreCase("")) {
            return response;
        }

        return "";
    }

    public static String getTimetable(Context context, String action, String userid) {
        List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>(3);
        try {
            nameValuePair.add(new BasicNameValuePair("action", action));
            nameValuePair.add(new BasicNameValuePair("user_id", userid));
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.d("xyz ",nameValuePair.toString());
        String response = RequestManager.callPost(URL_PREFIX, nameValuePair);
        if (response != null && !response.equalsIgnoreCase("")) {
            return response;
        }

        return "";
    }
}
