package com.example.sai.login_form.services;//package com.vahinfosol.www.stock_market.services;
//
//import java.lang.reflect.Type;
//
//import org.json.JSONObject;
//
//import android.content.Context;
//import android.content.Intent;
//import android.content.SharedPreferences;
//import android.os.AsyncTask;
//import android.os.Bundle;
//import android.support.v4.app.Fragment;
//import android.text.Html;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.view.ViewGroup;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//
//import com.google.analytics.tracking.android.EasyTracker;
//import com.google.gson.Gson;
//import com.google.gson.JsonElement;
//import com.google.gson.JsonParser;
//import com.google.gson.reflect.TypeToken;
//import com.sromku.simple.fb.Permission;
//import com.sromku.simple.fb.SimpleFacebook;
//import com.sromku.simple.fb.entities.Profile;
//import com.sromku.simple.fb.listeners.OnLoginListener;
//import com.sromku.simple.fb.listeners.OnProfileListener;
//import com.unoapp.homedelivery.Utils.Constant;
//import com.unoapp.homedelivery.Utils.FacebookUtils;
//import com.unoapp.homedelivery.Utils.HomeDeliveryUtils;
//import com.unoapp.homedelivery.objects.UserdetailObject;
//import com.unoapp.homedelivery.services.Services;
//
//public class SignInFragment extends Fragment {
//
//	// Titlebar
//	LinearLayout uperTitlebarLinearLayout, locationTitlebarLinearLayout;
//	TextView titleTextview, locationTextview, statusTextview,
//			selectedItemCountTextView;
//	ImageView slidDrawerImageView, orderImageView, locationPointImageView,
//			editLocationImageView, backImageView;
//
//	EditText emailAddressTypefacedEditText, passwordTypefacedEditText;
//	Button signInButton;
//	LinearLayout registerLinearLayout, facebookLoginLinearLayout,
//			guessLinearLayout;
//	String emailId, password;
//	private SimpleFacebook mSimpleFacebook;
//	boolean shouldLoadAgain = true;
//	SharedPreferences userSharedPreferences;
//
//	String user_location, uniqueId;
//	int orderItemCount;
//	UserdetailObject arUserDetailList;
//
//	SharedPreferences userDetailSharedPreferences;
//	TextView signInToProceedTextview, registerTextView;
//	TextView newUserTextView, facebookLoginTextView, guessTypefacedTextView,
//			orTextView;
//
//	// protected void onCreate(Bundle savedInstanceState) {
//	// super.onCreate(savedInstanceState);
//	// this.requestWindowFeature(Window.FEATURE_NO_TITLE);
//	// // this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//	// // WindowManager.LayoutParams.FLAG_FULLSCREEN);
//	// setContentView(R.layout.activity_signin);
//	//
//	// // if (savedInstanceState == null) {
//	// // getSupportFragmentManager().beginTransaction()
//	// // .add(R.id.container, new PlaceholderFragment()).commit();
//	// // }
//	//
//	// initializingTitlebar();
//	// initialization();
//	//
//	// }
//
//	@Override
//	public View onCreateView(LayoutInflater inflater, ViewGroup container,
//			Bundle savedInstanceState) {
//
//		View v = inflater.inflate(R.layout.activity_signin, container, false);
//
//		return v;
//	}
//
//	@Override
//	public void onStart() {
//		// TODO Auto-generated method stub
//		super.onStart();
//		EasyTracker.getInstance(getActivity()).activityStop(getActivity());
//		getCountvalue();
//	}
//
//	@Override
//	public void onStop() {
//		super.onStop();
//		EasyTracker.getInstance(getActivity()).activityStop(getActivity());
//	}
//
//	@Override
//	public void onResume() {
//
//		super.onResume();
//		getCountvalue();
//	}
//
//	@Override
//	public void onActivityCreated(Bundle savedInstanceState) {
//		super.onActivityCreated(savedInstanceState);
//
//		initializingTitlebar();
//		initialization();
//	}
//
//	protected Context getContext() {
//		return getActivity();
//	}
//
//	private void getCountvalue() {
//
//		orderItemCount = HomeDeliveryUtils.getCartValue(getContext());
//
//		if (orderItemCount > 0) {
//			selectedItemCountTextView.setVisibility(View.VISIBLE);
//			selectedItemCountTextView.setText(String.format("%02d",
//					orderItemCount));
//		} else {
//			selectedItemCountTextView.setVisibility(View.GONE);
//		}
//	}
//
//	private void initializingTitlebar() {
//
//		uperTitlebarLinearLayout = (LinearLayout) getActivity().findViewById(
//				R.id.uperTitlebarLinearLayout);
//		locationTitlebarLinearLayout = (LinearLayout) getActivity()
//				.findViewById(R.id.locationTitlebarLinearLayout);
//
//		editLocationImageView = (ImageView) getActivity().findViewById(
//				R.id.editLocationImageView);
//		locationPointImageView = (ImageView) getActivity().findViewById(
//				R.id.locationPointImageView);
//
//		titleTextview = (TextView) getActivity().findViewById(
//				R.id.titleTextview);
//		titleTextview.setText(HomeDeliveryUtils.getLocalString(getContext(),
//				"cart", HomeDeliveryUtils.getLanguage(getContext())));
//		// titleTextview.setText(getResources().getString(R.string.cart));
//		locationTextview = (TextView) getActivity().findViewById(
//				R.id.locationTextview);
//		statusTextview = (TextView) getActivity().findViewById(
//				R.id.statusTextview);
//		statusTextview.setVisibility(View.GONE);
//
//		slidDrawerImageView = (ImageView) getActivity().findViewById(
//				R.id.slidDrawerImageView);
//		slidDrawerImageView.setVisibility(View.GONE);
//		orderImageView = (ImageView) getActivity().findViewById(
//				R.id.orderImageView);
//		backImageView = (ImageView) getActivity().findViewById(
//				R.id.backImageView);
//		backImageView.setVisibility(View.VISIBLE);
//
//		selectedItemCountTextView = (TextView) getActivity().findViewById(
//				R.id.selectedItemCountTextView);
//
//	}
//
//	private void initialization() {
//
//		user_location = HomeDeliveryUtils.getGpsLocation(getContext());
//		locationTextview.setText(user_location.toString());
//		signInToProceedTextview = (TextView) getActivity().findViewById(
//				R.id.signInToProceedTextview);
//		signInToProceedTextview.setText(HomeDeliveryUtils.getLocalString(
//				getContext(), "please_sign_in_to_proceed",
//				HomeDeliveryUtils.getLanguage(getContext())));
//		uniqueId = HomeDeliveryUtils.getUniqueId(getContext());
//		// Log.e("uniqueId......", uniqueId);
//
//		emailAddressTypefacedEditText = (EditText) getActivity().findViewById(
//				R.id.emailAddressTypefacedEditText);
//		emailAddressTypefacedEditText.setHint(HomeDeliveryUtils.getLocalString(
//				getContext(), "email_address",
//				HomeDeliveryUtils.getLanguage(getContext())));
//
//		passwordTypefacedEditText = (EditText) getActivity().findViewById(
//				R.id.passwordTypefacedEditText);
//		passwordTypefacedEditText.setHint(HomeDeliveryUtils.getLocalString(
//				getContext(), "password",
//				HomeDeliveryUtils.getLanguage(getContext())));
//
//		signInButton = (Button) getActivity().findViewById(R.id.signInButton);
//
//		signInButton.setText(HomeDeliveryUtils.getLocalString(getContext(),
//				"sign_in", HomeDeliveryUtils.getLanguage(getContext())));
//
//		registerTextView = (TextView) getActivity().findViewById(
//				R.id.registerTextView);
//
//		registerTextView.setText(HomeDeliveryUtils.getLocalString(getContext(),
//				"register_now", HomeDeliveryUtils.getLanguage(getContext())));
//
//		newUserTextView = (TextView) getActivity().findViewById(
//				R.id.newUserTextView);
//		newUserTextView.setText(HomeDeliveryUtils.getLocalString(getContext(),
//				"new_user", HomeDeliveryUtils.getLanguage(getContext())));
//
//		registerLinearLayout = (LinearLayout) getActivity().findViewById(
//				R.id.registerLinearLayout);
//
//		facebookLoginLinearLayout = (LinearLayout) getActivity().findViewById(
//				R.id.facebookLoginLinearLayout);
//		facebookLoginTextView = (TextView) getActivity().findViewById(
//				R.id.facebookLoginTextView);
//		facebookLoginTextView.setText(HomeDeliveryUtils.getLocalString(
//				getContext(), "facebook_login",
//				HomeDeliveryUtils.getLanguage(getContext())));
//
//		guessLinearLayout = (LinearLayout) getActivity().findViewById(
//				R.id.proceedAsGuessLinearLayout);
//		guessTypefacedTextView = (TextView) getActivity().findViewById(
//				R.id.guessTypefacedTextView);
//		guessTypefacedTextView.setText(HomeDeliveryUtils.getLocalString(
//				getContext(), "proceed_as_a_guest",
//				HomeDeliveryUtils.getLanguage(getContext())));
//
//		orTextView = (TextView) getActivity().findViewById(R.id.orTextView);
//		orTextView.setText(HomeDeliveryUtils.getLocalString(getContext(), "or",
//				HomeDeliveryUtils.getLanguage(getContext())));
//
//		onClickMethods();
//	}
//
//	private void onClickMethods() {
//
//		editLocationImageView.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				// showMyLocationScreen();
//				showLocationScreen();
//
//			}
//		});
//
//		locationPointImageView.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				// showLocationScreen();
//				showMyLocationScreen();
//
//			}
//		});
//
//		signInButton.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View arg0) {
//
//				// tostMsg("Click signin");
//
//				emailId = emailAddressTypefacedEditText.getText().toString();
//				password = passwordTypefacedEditText.getText().toString();
//
//				checkUserData();
//			}
//		});
//
//		registerLinearLayout.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View arg0) {
//
//				// tostMsg("Click register");
//				onRegister();
//			}
//		});
//
//		facebookLoginLinearLayout.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View arg0) {
//
//				// tostMsg("Remaining Facebook");
//				// onFacebookLogin();
//
//				((FragmentContainerActivity) getActivity()).onFacebookLogin();
//			}
//		});
//
//		guessLinearLayout.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View arg0) {
//
//				// tostMsg("Click user as guess");
//				onGuestSignIn();
//			}
//		});
//
//		orderImageView.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View arg0) {
//
//				// tostMsg("Click on Order");
//				onOrderCart();
//			}
//		});
//
//		backImageView.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View arg0) {
//
//				// tostMsg("Click on Back");
//				onToggleMenu();
//			}
//		});
//
//	}
//
//	protected void showLocationScreen() {
//
//		startActivity(new Intent(getContext(), LocationViewActivity.class));
//	}
//
//	protected void showMyLocationScreen() {
//		Intent mIntent = new Intent(getContext(),
//				FragmentContainerActivity.class);
//		mIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
//		mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//		mIntent.putExtra("fragmentNumber", "4"); // for example
//		startActivity(mIntent);
//
//	}
//
//	protected void onOrderCart() {
//		showOrderCartScreen();
//
//	}
//
//	private void showOrderCartScreen() {
//
//		if (orderItemCount > 0) {
//			startActivity(new Intent(getContext(), ShoppingCartActivity.class));
//		} else {
//			HomeDeliveryUtils.openDialog(getContext(), getContext()
//					.getResources().getString(R.string.cart_is_empty));
//		}
//	}
//
//	protected void onGuestSignIn() {
//		startActivity(new Intent(getContext(), GuestSignInActivity.class));
//
//	}
//
//	protected void onRegister() {
//
//		startActivity(new Intent(getContext(), RegistrationActivity.class));
//	}
//
//	protected void onFacebookLogin() {
//
//		// startActivity(new Intent(getApplicationContext(),
//		// ProfileActivity.class));
//		mSimpleFacebook.login(onLoginListener);
//
//	}
//
//	protected void checkUserData() {
//
//		if (emailId.equals("")) {
//			tostMsg("Enter mail");
//
//		} else if (password.equals("")) {
//
//			tostMsg("Enter password");
//
//		} else {
//			// tostMsg("Check USER Data");
//
//			showUserProfile();
//		}
//
//	}
//
//	private void showUserProfile() {
//
//		AsyncTaskretriveUserDetail asyncRetriveUserDetailTask = new AsyncTaskretriveUserDetail();
//		asyncRetriveUserDetailTask.execute();
//
//		// startActivity(new Intent(getApplicationContext(),
//		// ProfileActivity.class));
//	}
//
//	private class AsyncTaskretriveUserDetail extends
//			AsyncTask<Void, Void, String> {
//
//		@Override
//		protected void onPreExecute() {
//			super.onPreExecute();
//
//			HomeDeliveryUtils.showProgress(getContext());
//			if (!(HomeDeliveryUtils.isInternetConnected(getContext()))) {
//
//				HomeDeliveryUtils.showAlertDialog(getContext());
//				System.out.println("No Internet Connection");
//				return;
//
//			}
//
//		}
//
//		@Override
//		protected String doInBackground(Void... params) {
//
//			String response = Services.doLogin(getContext(),
//					HomeDeliveryUtils.getCompanyId(getContext()),
//					Constant.DEVICE_ID, emailId, password);
//
//			System.out.println(response);
//
//			return response;
//		}
//
//		@Override
//		protected void onPostExecute(String response) {
//			Log.d("result " + response, response);
//			HomeDeliveryUtils.dimissProgress();
//			parseSignInUserDetailResponse(response);
//		}
//	}
//
//	public void parseSignInUserDetailResponse(String response) {
//
//		if (response != null && !response.equals("")) {
//			try {
//				JSONObject jsonobj = new JSONObject(response);
//
//				if (jsonobj.has(Constant.RESULT)) {
//					if (jsonobj.getString(Constant.RESULT).equalsIgnoreCase(
//							Constant.SUCCESS)) {
//
//						if (jsonobj.has(Constant.DATA)) {
//							JSONObject flagObject = jsonobj
//									.getJSONObject(Constant.DATA);
//
//							if (flagObject.getString(Constant.FLAG)
//									.equalsIgnoreCase("true")) {
//
//								JSONObject userArray = flagObject
//										.getJSONObject(Constant.DATA);
//
//								JsonElement json = new JsonParser()
//										.parse(userArray.toString());
//
//								Type type = new TypeToken<UserdetailObject>() {
//								}.getType();
//
//								arUserDetailList = new Gson().fromJson(json,
//										type);
//
//								userSharedPreferences = getContext()
//										.getSharedPreferences(
//												Constant.SHARED_PREFERENCES_USER_DETAILS,
//												0);
//								SharedPreferences.Editor editor = userSharedPreferences
//										.edit();
//								editor.putString(Constant.USER_ID,
//										arUserDetailList.getUser_id());
//								editor.putString(Constant.FIRST_NAME,
//										arUserDetailList.getFirst_name());
//								editor.putString(Constant.LAST_NAME,
//										arUserDetailList.getLast_name());
//								editor.putString(Constant.EMAIL_ID,
//										arUserDetailList.getEmail_id());
//								editor.putString(Constant.PHONE,
//										arUserDetailList.getPhone());
//								editor.putString(Constant.DATE_OF_BIRTH,
//										arUserDetailList.getDate_of_birth());
//								editor.putString(Constant.USER_IMAGE_URL,
//										arUserDetailList.getImage());
//								// editor.putString(Constant.EMAIL_MARKETTING,
//								// arUserDetailList.get);
//
//								editor.commit();
//
//								if (HomeDeliveryUtils.getUserId(getContext())
//										.equalsIgnoreCase("")
//										&& HomeDeliveryUtils.getUserId(
//												getContext()).length() == 0) {
//
//								} else {
//									FragmentContainerActivity
//											.registerUserForPushNotification(
//													HomeDeliveryUtils
//															.getUserId(getContext()),
//													getActivity());
//								}
//
//								checkUniqueId();
//
//							} else {
//								System.out.println("false");
//
//								HomeDeliveryUtils
//										.openDialog(
//												getContext(),
//												getContext()
//														.getResources()
//														.getString(
//																R.string.alert_check_username_password));
//
//								// HomeDeliveryUtils.showAlertDialog(
//								// getContext(),
//								// getContext()
//								// .getResources()
//								// .getString(
//								// R.string.alert_check_username_password));
//							}
//						}
//					}
//				} else {
//
//				}
//
//			} catch (Exception e) {
//				e.printStackTrace();
//
//			}
//
//		}
//	}
//
//	private void checkUniqueId() {
//
//		if (uniqueId.equals("") || uniqueId.equals("0")) {
//			onMainScreen();
//		} else {
//			AsyncTaskasignLocationToUser asyncTaskasignLocationToUser = new AsyncTaskasignLocationToUser();
//			asyncTaskasignLocationToUser.execute();
//
//		}
//
//	}
//
//	private class AsyncTaskasignLocationToUser extends
//			AsyncTask<Void, Void, String> {
//
//		@Override
//		protected void onPreExecute() {
//			super.onPreExecute();
//
//			HomeDeliveryUtils.showProgress(getContext());
//			if (!(HomeDeliveryUtils.isInternetConnected(getContext()))) {
//
//				HomeDeliveryUtils.showAlertDialog(getContext());
//				System.out.println("No Internet Connection");
//				return;
//
//			}
//
//		}
//
//		@Override
//		protected String doInBackground(Void... params) {
//
//			String response = Services.assignLocationToUser(getContext(),
//					HomeDeliveryUtils.getCompanyId(getContext()),
//					HomeDeliveryUtils.getDeviceId(getContext()), uniqueId,
//					HomeDeliveryUtils.getUserId(getContext()));
//
//			System.out.println(response);
//
//			return response;
//		}
//
//		@Override
//		protected void onPostExecute(String response) {
//			Log.d("result " + response, response);
//			HomeDeliveryUtils.dimissProgress();
//			// onMainScreen();
//			parseasignUserLocationResponse(response);
//		}
//	}
//
//	public void parseasignUserLocationResponse(String response) {
//
//		if (response != null && !response.equals("")) {
//			try {
//				JSONObject jsonobj = new JSONObject(response);
//
//				if (jsonobj.has(Constant.RESULT)) {
//					if (jsonobj.getString(Constant.RESULT).equalsIgnoreCase(
//							Constant.SUCCESS)) {
//
//						if (jsonobj.has(Constant.DATA)) {
//							JSONObject flagObject = jsonobj
//									.getJSONObject(Constant.DATA);
//
//							if (flagObject.getString(Constant.FLAG)
//									.equalsIgnoreCase("true")) {
//
//								SharedPreferences preferences = getContext()
//										.getSharedPreferences(
//												Constant.SHARED_PREFERENCES_USER_DETAILS,
//												0);
//								preferences.edit().remove(Constant.UNIQUE_ID)
//										.commit();
//
//								onMainScreen();
//
//							} else {
//								System.out.println("false");
//								onMainScreen();
//							}
//						}
//					}
//				} else {
//
//				}
//
//			} catch (Exception e) {
//				e.printStackTrace();
//
//			}
//
//		}
//	}
//
//	public void onMainScreen() {
//
//		SharedPreferences sharedPreferences = getContext()
//				.getSharedPreferences(
//						Constant.SHARED_PREFERENCES_OTHER_DETAILS, 0);
//		sharedPreferences.getString(Constant.OPEN_FINAL_CONFIRMATION, "");
//		if (sharedPreferences.getString(Constant.OPEN_FINAL_CONFIRMATION, "")
//				.equals("true")) {
//			startActivity(new Intent(getContext(), PlaceOrderActivity.class));
//			getActivity().finish();
//		} else {
//			Intent mIntent = new Intent(getContext(),
//					FragmentContainerActivity.class);
//			startActivity(mIntent);
//			getActivity().finish();
//		}
//
//	}
//
//	private void tostMsg(String msg) {
//
//		HomeDeliveryUtils.showToast(getContext(), msg);
//	}
//
//	OnLoginListener onLoginListener = new OnLoginListener() {
//
//		@Override
//		public void onFail(String reason) {
//			System.out.println("reason  ---- " + reason);
//		}
//
//		@Override
//		public void onException(Throwable throwable) {
//			System.out.println("throwable  ---- " + throwable);
//			if (shouldLoadAgain) {
//				shouldLoadAgain = false;
//				mSimpleFacebook.login(onLoginListener);
//			}
//		}
//
//		@Override
//		public void onThinking() {
//			// show progress bar or something to the user while login is
//			// happening
//			System.out.println("onThinking");
//			loggedInUIState();
//
//		}
//
//		@Override
//		public void onLogin() {
//			// change the state of the button or do whatever you want
//			System.out.println("onLogin");
//			// loggedInUIState();
//		}
//
//		@Override
//		public void onNotAcceptingPermissions(Permission.Type type) {
//			System.out.println("onNotAcceptingPermissions  ---- " + type);
//
//		}
//	};
//
//	protected void loggedInUIState() {
//		onRetrievingFacebookProfileDetails();
//
//	}
//
//	private void onRetrievingFacebookProfileDetails() {
//		try {
//
//			// SimpleFacebook.getInstance().getPhotos("", new OnPhotosListener()
//			// {
//			// });
//			SimpleFacebook.getInstance().getProfile(new OnProfileListener() {
//
//				@Override
//				public void onThinking() {
//					// showDialog("Please wait...",
//					// "Retrieving facebook profile...");
//				}
//
//				@Override
//				public void onException(Throwable throwable) {
//					// hideDialog();
//					Log.e("dax",
//							"getProfile onException " + throwable.getMessage());
//					// detailsTextView.setText(throwable.getMessage());
//				}
//
//				@Override
//				public void onFail(String reason) {
//					// hideDialog();
//					Log.e("dax", "getProfile onFail " + reason);
//					// detailsTextView.setText(reason);
//				}
//
//				@Override
//				public void onComplete(Profile response) {
//					try {
//						String str = FacebookUtils.toHtml(response);
//						Log.e("dax",
//								"getProfiles onResponse " + Html.fromHtml(str));
//
//						String firstName = response.getFirstName();
//						String lastName = response.getLastName();
//						String email = response.getEmail();
//						Log.e("firstName==", firstName);
//						Log.e("lastName==", lastName);
//						Log.e("email==", email);
//						// fullName = firstName + " " + lastName;
//						// facebookBirthDate = response.getBirthday();
//						// System.out.println("facebookBirthDate == "
//						// + facebookBirthDate);
//						// // userNameAgeTextview.setText(fullName + ", " +
//						// age);
//						// userNameAgeTextview.setText(fullName);
//						// finalAge = age;
//
//						// if (facebookBirthDate.toString() != null
//						// && facebookBirthDate.equalsIgnoreCase("")) {
//						//
//						// userNameAgeTextview.setText(fullName + ", " + age);
//						// finalAge = age;
//						// System.out.println("finalAge1 =="+finalAge);
//						// } else {
//						// fbAge = ageValidation(facebookBirthDate);
//						// System.out.println(fbAge);
//						// userNameAgeTextview
//						// .setText(fullName + ", " + fbAge);
//						// finalAge = fbAge;
//						// System.out.println("finalAge2 =="+finalAge);
//						// }
//
//						// location_facebook = "";
//
//						// location_facebook = response.getLocation().getName();
//						//
//						// if (location_facebook == null
//						// || location_facebook.equalsIgnoreCase("")) {
//						// // location_facebook = "";
//						// // get location from gps
//						// System.out.println("GPSLocation 111111111==== "+GPSLocation);
//						// location_facebook = GPSLocation;
//						//
//						// }
//						// // location_facebook =
//						// response.getLocation().getName()
//						// // .toString();
//						// else {
//						//
//						// }
//						//
//						// System.out.println("GPSLocation 22222222222==== "+GPSLocation);
//						// locationTextview.setText("From : " +
//						// location_facebook);
//						//
//						// facebookId = response.getId();
//						// facebookEmail = response.getEmail();
//
//						// System.out.println("getProfiles response ==== " +
//						// str);
//
//						// new AsyncTaskRetrieveProfileImage().execute(response
//						// .getId());
//
//						// profileBitmap =
//						// getFacebookProfilePicture(response.getId());
//						// profilePicImageview.setImageBitmap(profileBitmap);
//						//
//						try {
//							String imageUrl = "https://graph.facebook.com/"
//									+ response.getId() + "/picture?type=large";
//							System.out.println("imageUrl ==" + imageUrl);
//							// showProfileImage(imageUrl);
//						} catch (Exception e) {
//							e.printStackTrace();
//						}
//					} catch (Exception e) {
//						e.printStackTrace();
//					}
//
//					// hideDialog();
//
//				}
//			});
//		} catch (NullPointerException e) {
//			e.printStackTrace();
//			System.out.println(e);
//
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//	}
//
//	protected void onToggleMenu() {
//		((FragmentContainerActivity) getActivity()).toggleSlidingMenu();
//	}
//
//}
